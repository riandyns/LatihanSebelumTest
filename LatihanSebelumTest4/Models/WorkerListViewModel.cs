﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanSebelumTest4.Models
{
    public class WorkerListViewModel
    {
        public Guid WorkerId { get; set; }

        public string Name { get; set; }

        public decimal Weight { get; set; }

        public bool Gender { get; set; }

    }
}
