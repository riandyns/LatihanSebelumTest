﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanSebelumTest4.Models
{
    public class CreateWorkerViewModel
    {

        [Required]
        public Guid WorkerId { set; get; }

        [Required]
        [StringLength(255)]
        public string Name { set; get; }
        
        [Required]
        [Range(30,100)]
        public decimal Weight { set; get; }

        [Required]
        public bool Gender { set; get; }

    }
}
