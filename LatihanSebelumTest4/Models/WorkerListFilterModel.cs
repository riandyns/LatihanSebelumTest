﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanSebelumTest4.Models
{
    public class WorkerListFilterModel
    {
        public string WN { get; set; }

        public bool G { get; set; }

        public int WE { set; get; }

        public string Or { get; set; }

        public string Gor { set; get; }

        public bool IDesc { get; set; }
    }
}
