﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanSebelumTest4.Models
{
    public class WorkerListGridViewModel
    {
        public List<WorkerListViewModel> Workers { get; set; }
    }
}
