﻿export interface IWorkerListFilterModel {

    wn: string;

    g: boolean;

    or: string;

    gor: string;

    iDesc: boolean;

}
