﻿export interface IWorkertListViewModel {
    workerId: string;

    workerName: string;

    weight: number;

    gender: boolean;
}