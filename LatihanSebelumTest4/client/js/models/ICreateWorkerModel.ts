﻿export interface ICreateWorkerModel {

    workerName: string;

    weight: number;

    gender: boolean;
}