﻿import Axios from 'axios';
import { IWorkertListViewModel } from '../models/IWorkerListViewModel';
import { IWorkerListGridViewModel } from '../models/IWorkerListGridViewModel';
import { IWorkerListFilterModel } from '../models/IWorkerListFilterModel';
import { ICreateWorkerModel } from '../models/ICreateWorkerModel';

export class WorkerListService {

    workerGrid: IWorkerListGridViewModel = {
        workers: []
    };

    async getWorkerGrid() {
        let response = await Axios.get<IWorkerListGridViewModel>('/api/v1/worker'
            , {
                params: this.workerFilter
            });

        this.workerGrid = response.data;
    }

    workerFilter: IWorkerListFilterModel = {
        wn: '',
        g: true,
        or: 'Worker Name',
        gor: 'Male',
        iDesc: false
    };

    createWorkerField: ICreateWorkerModel = {
        workerName: '',
        weight: 1,
        gender: true
    };

    async createWorker() {

        await Axios.post<string>('/api/v1/worker', this.createWorkerField);

        //this.resetCreateField();

        await this.getWorkerGrid();
    }

    orderByOptions = ['Worker Name', 'Weight', 'Gender'];
    getGenderByOptions = ['Male', 'Female', 'All'];

}

export let WorkerListServiceSingleton = new WorkerListService();