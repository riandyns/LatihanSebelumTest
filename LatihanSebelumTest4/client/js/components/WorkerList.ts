﻿import Vue from 'vue';
import Component from 'vue-class-component';
import { render, staticRenderFns } from './WorkerList.vue.html';
import { WorkerListServiceSingleton } from '../services/WorkerListService';
import { IWorkerListFilterModel } from '../models/IWorkerListFilterModel';
import { ICreateWorkerModel } from '../models/ICreateWorkerModel';

@Component({
    render, staticRenderFns,
    created: async function(this: WorkerList) {
        await this.workerListState.getWorkerGrid();
    }
})

export class WorkerList extends Vue {
    workerListState = WorkerListServiceSingleton;

    get getOrderByOptions() {
        return this.workerListState.orderByOptions;
    }

    get getGenderByOptions() {
        return this.workerListState.getGenderByOptions;
    }

    createField: ICreateWorkerModel = {
        workerName: '',
        weight: 1,
        gender: true
    }

    filter: IWorkerListFilterModel = {
        wn: '',
        g: true,
        or: 'Worker Name',
        gor: 'Male',
        iDesc: false
    };

    async search() {

        this.workerListState.workerFilter = this.filter;

        await this.workerListState.getWorkerGrid();
    }

    async createWorker() {
        this.workerListState.createWorkerField = this.createField;

        await this.workerListState.createWorker();

        this.createField = {
            workerName: '',
            weight: 1,
            gender: true
        };
    }

    get getWorkerGrid() {
        return this.workerListState.workerGrid;
    }
}