import Vue from 'vue';
import * as Components from './components';
import { WorkerList } from './components/WorkerList';

// components must be registered BEFORE the app root declaration
Vue.component('hello', Components.Hello);
Vue.component('worker-list', WorkerList);

// bootstrap the Vue app from the root element <div id="app"></div>
new Vue().$mount('#app');
