(function(global, factory) {
    typeof exports === "object" && typeof module !== "undefined" ? module.exports = factory() : typeof define === "function" && define.amd ? define(factory) : global.ES6Promise = factory();
})(this, function() {
    "use strict";
    function objectOrFunction(x) {
        var type = typeof x;
        return x !== null && (type === "object" || type === "function");
    }
    function isFunction(x) {
        return typeof x === "function";
    }
    var _isArray = void 0;
    if (Array.isArray) {
        _isArray = Array.isArray;
    } else {
        _isArray = function(x) {
            return Object.prototype.toString.call(x) === "[object Array]";
        };
    }
    var isArray = _isArray;
    var len = 0;
    var vertxNext = void 0;
    var customSchedulerFn = void 0;
    var asap = function asap(callback, arg) {
        queue[len] = callback;
        queue[len + 1] = arg;
        len += 2;
        if (len === 2) {
            if (customSchedulerFn) {
                customSchedulerFn(flush);
            } else {
                scheduleFlush();
            }
        }
    };
    function setScheduler(scheduleFn) {
        customSchedulerFn = scheduleFn;
    }
    function setAsap(asapFn) {
        asap = asapFn;
    }
    var browserWindow = typeof window !== "undefined" ? window : undefined;
    var browserGlobal = browserWindow || {};
    var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
    var isNode = typeof self === "undefined" && typeof process !== "undefined" && {}.toString.call(process) === "[object process]";
    var isWorker = typeof Uint8ClampedArray !== "undefined" && typeof importScripts !== "undefined" && typeof MessageChannel !== "undefined";
    function useNextTick() {
        return function() {
            return process.nextTick(flush);
        };
    }
    function useVertxTimer() {
        if (typeof vertxNext !== "undefined") {
            return function() {
                vertxNext(flush);
            };
        }
        return useSetTimeout();
    }
    function useMutationObserver() {
        var iterations = 0;
        var observer = new BrowserMutationObserver(flush);
        var node = document.createTextNode("");
        observer.observe(node, {
            characterData: true
        });
        return function() {
            node.data = iterations = ++iterations % 2;
        };
    }
    function useMessageChannel() {
        var channel = new MessageChannel();
        channel.port1.onmessage = flush;
        return function() {
            return channel.port2.postMessage(0);
        };
    }
    function useSetTimeout() {
        var globalSetTimeout = setTimeout;
        return function() {
            return globalSetTimeout(flush, 1);
        };
    }
    var queue = new Array(1e3);
    function flush() {
        for (var i = 0; i < len; i += 2) {
            var callback = queue[i];
            var arg = queue[i + 1];
            callback(arg);
            queue[i] = undefined;
            queue[i + 1] = undefined;
        }
        len = 0;
    }
    function attemptVertx() {
        try {
            var vertx = Function("return this")().require("vertx");
            vertxNext = vertx.runOnLoop || vertx.runOnContext;
            return useVertxTimer();
        } catch (e) {
            return useSetTimeout();
        }
    }
    var scheduleFlush = void 0;
    if (isNode) {
        scheduleFlush = useNextTick();
    } else if (BrowserMutationObserver) {
        scheduleFlush = useMutationObserver();
    } else if (isWorker) {
        scheduleFlush = useMessageChannel();
    } else if (browserWindow === undefined && typeof require === "function") {
        scheduleFlush = attemptVertx();
    } else {
        scheduleFlush = useSetTimeout();
    }
    function then(onFulfillment, onRejection) {
        var parent = this;
        var child = new this.constructor(noop);
        if (child[PROMISE_ID] === undefined) {
            makePromise(child);
        }
        var _state = parent._state;
        if (_state) {
            var callback = arguments[_state - 1];
            asap(function() {
                return invokeCallback(_state, child, callback, parent._result);
            });
        } else {
            subscribe(parent, child, onFulfillment, onRejection);
        }
        return child;
    }
    function resolve$1(object) {
        var Constructor = this;
        if (object && typeof object === "object" && object.constructor === Constructor) {
            return object;
        }
        var promise = new Constructor(noop);
        resolve(promise, object);
        return promise;
    }
    var PROMISE_ID = Math.random().toString(36).substring(2);
    function noop() {}
    var PENDING = void 0;
    var FULFILLED = 1;
    var REJECTED = 2;
    var TRY_CATCH_ERROR = {
        error: null
    };
    function selfFulfillment() {
        return new TypeError("You cannot resolve a promise with itself");
    }
    function cannotReturnOwn() {
        return new TypeError("A promises callback cannot return that same promise.");
    }
    function getThen(promise) {
        try {
            return promise.then;
        } catch (error) {
            TRY_CATCH_ERROR.error = error;
            return TRY_CATCH_ERROR;
        }
    }
    function tryThen(then$$1, value, fulfillmentHandler, rejectionHandler) {
        try {
            then$$1.call(value, fulfillmentHandler, rejectionHandler);
        } catch (e) {
            return e;
        }
    }
    function handleForeignThenable(promise, thenable, then$$1) {
        asap(function(promise) {
            var sealed = false;
            var error = tryThen(then$$1, thenable, function(value) {
                if (sealed) {
                    return;
                }
                sealed = true;
                if (thenable !== value) {
                    resolve(promise, value);
                } else {
                    fulfill(promise, value);
                }
            }, function(reason) {
                if (sealed) {
                    return;
                }
                sealed = true;
                reject(promise, reason);
            }, "Settle: " + (promise._label || " unknown promise"));
            if (!sealed && error) {
                sealed = true;
                reject(promise, error);
            }
        }, promise);
    }
    function handleOwnThenable(promise, thenable) {
        if (thenable._state === FULFILLED) {
            fulfill(promise, thenable._result);
        } else if (thenable._state === REJECTED) {
            reject(promise, thenable._result);
        } else {
            subscribe(thenable, undefined, function(value) {
                return resolve(promise, value);
            }, function(reason) {
                return reject(promise, reason);
            });
        }
    }
    function handleMaybeThenable(promise, maybeThenable, then$$1) {
        if (maybeThenable.constructor === promise.constructor && then$$1 === then && maybeThenable.constructor.resolve === resolve$1) {
            handleOwnThenable(promise, maybeThenable);
        } else {
            if (then$$1 === TRY_CATCH_ERROR) {
                reject(promise, TRY_CATCH_ERROR.error);
                TRY_CATCH_ERROR.error = null;
            } else if (then$$1 === undefined) {
                fulfill(promise, maybeThenable);
            } else if (isFunction(then$$1)) {
                handleForeignThenable(promise, maybeThenable, then$$1);
            } else {
                fulfill(promise, maybeThenable);
            }
        }
    }
    function resolve(promise, value) {
        if (promise === value) {
            reject(promise, selfFulfillment());
        } else if (objectOrFunction(value)) {
            handleMaybeThenable(promise, value, getThen(value));
        } else {
            fulfill(promise, value);
        }
    }
    function publishRejection(promise) {
        if (promise._onerror) {
            promise._onerror(promise._result);
        }
        publish(promise);
    }
    function fulfill(promise, value) {
        if (promise._state !== PENDING) {
            return;
        }
        promise._result = value;
        promise._state = FULFILLED;
        if (promise._subscribers.length !== 0) {
            asap(publish, promise);
        }
    }
    function reject(promise, reason) {
        if (promise._state !== PENDING) {
            return;
        }
        promise._state = REJECTED;
        promise._result = reason;
        asap(publishRejection, promise);
    }
    function subscribe(parent, child, onFulfillment, onRejection) {
        var _subscribers = parent._subscribers;
        var length = _subscribers.length;
        parent._onerror = null;
        _subscribers[length] = child;
        _subscribers[length + FULFILLED] = onFulfillment;
        _subscribers[length + REJECTED] = onRejection;
        if (length === 0 && parent._state) {
            asap(publish, parent);
        }
    }
    function publish(promise) {
        var subscribers = promise._subscribers;
        var settled = promise._state;
        if (subscribers.length === 0) {
            return;
        }
        var child = void 0, callback = void 0, detail = promise._result;
        for (var i = 0; i < subscribers.length; i += 3) {
            child = subscribers[i];
            callback = subscribers[i + settled];
            if (child) {
                invokeCallback(settled, child, callback, detail);
            } else {
                callback(detail);
            }
        }
        promise._subscribers.length = 0;
    }
    function tryCatch(callback, detail) {
        try {
            return callback(detail);
        } catch (e) {
            TRY_CATCH_ERROR.error = e;
            return TRY_CATCH_ERROR;
        }
    }
    function invokeCallback(settled, promise, callback, detail) {
        var hasCallback = isFunction(callback), value = void 0, error = void 0, succeeded = void 0, failed = void 0;
        if (hasCallback) {
            value = tryCatch(callback, detail);
            if (value === TRY_CATCH_ERROR) {
                failed = true;
                error = value.error;
                value.error = null;
            } else {
                succeeded = true;
            }
            if (promise === value) {
                reject(promise, cannotReturnOwn());
                return;
            }
        } else {
            value = detail;
            succeeded = true;
        }
        if (promise._state !== PENDING) {} else if (hasCallback && succeeded) {
            resolve(promise, value);
        } else if (failed) {
            reject(promise, error);
        } else if (settled === FULFILLED) {
            fulfill(promise, value);
        } else if (settled === REJECTED) {
            reject(promise, value);
        }
    }
    function initializePromise(promise, resolver) {
        try {
            resolver(function resolvePromise(value) {
                resolve(promise, value);
            }, function rejectPromise(reason) {
                reject(promise, reason);
            });
        } catch (e) {
            reject(promise, e);
        }
    }
    var id = 0;
    function nextId() {
        return id++;
    }
    function makePromise(promise) {
        promise[PROMISE_ID] = id++;
        promise._state = undefined;
        promise._result = undefined;
        promise._subscribers = [];
    }
    function validationError() {
        return new Error("Array Methods must be provided an Array");
    }
    var Enumerator = function() {
        function Enumerator(Constructor, input) {
            this._instanceConstructor = Constructor;
            this.promise = new Constructor(noop);
            if (!this.promise[PROMISE_ID]) {
                makePromise(this.promise);
            }
            if (isArray(input)) {
                this.length = input.length;
                this._remaining = input.length;
                this._result = new Array(this.length);
                if (this.length === 0) {
                    fulfill(this.promise, this._result);
                } else {
                    this.length = this.length || 0;
                    this._enumerate(input);
                    if (this._remaining === 0) {
                        fulfill(this.promise, this._result);
                    }
                }
            } else {
                reject(this.promise, validationError());
            }
        }
        Enumerator.prototype._enumerate = function _enumerate(input) {
            for (var i = 0; this._state === PENDING && i < input.length; i++) {
                this._eachEntry(input[i], i);
            }
        };
        Enumerator.prototype._eachEntry = function _eachEntry(entry, i) {
            var c = this._instanceConstructor;
            var resolve$$1 = c.resolve;
            if (resolve$$1 === resolve$1) {
                var _then = getThen(entry);
                if (_then === then && entry._state !== PENDING) {
                    this._settledAt(entry._state, i, entry._result);
                } else if (typeof _then !== "function") {
                    this._remaining--;
                    this._result[i] = entry;
                } else if (c === Promise$2) {
                    var promise = new c(noop);
                    handleMaybeThenable(promise, entry, _then);
                    this._willSettleAt(promise, i);
                } else {
                    this._willSettleAt(new c(function(resolve$$1) {
                        return resolve$$1(entry);
                    }), i);
                }
            } else {
                this._willSettleAt(resolve$$1(entry), i);
            }
        };
        Enumerator.prototype._settledAt = function _settledAt(state, i, value) {
            var promise = this.promise;
            if (promise._state === PENDING) {
                this._remaining--;
                if (state === REJECTED) {
                    reject(promise, value);
                } else {
                    this._result[i] = value;
                }
            }
            if (this._remaining === 0) {
                fulfill(promise, this._result);
            }
        };
        Enumerator.prototype._willSettleAt = function _willSettleAt(promise, i) {
            var enumerator = this;
            subscribe(promise, undefined, function(value) {
                return enumerator._settledAt(FULFILLED, i, value);
            }, function(reason) {
                return enumerator._settledAt(REJECTED, i, reason);
            });
        };
        return Enumerator;
    }();
    function all(entries) {
        return new Enumerator(this, entries).promise;
    }
    function race(entries) {
        var Constructor = this;
        if (!isArray(entries)) {
            return new Constructor(function(_, reject) {
                return reject(new TypeError("You must pass an array to race."));
            });
        } else {
            return new Constructor(function(resolve, reject) {
                var length = entries.length;
                for (var i = 0; i < length; i++) {
                    Constructor.resolve(entries[i]).then(resolve, reject);
                }
            });
        }
    }
    function reject$1(reason) {
        var Constructor = this;
        var promise = new Constructor(noop);
        reject(promise, reason);
        return promise;
    }
    function needsResolver() {
        throw new TypeError("You must pass a resolver function as the first argument to the promise constructor");
    }
    function needsNew() {
        throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
    }
    var Promise$2 = function() {
        function Promise(resolver) {
            this[PROMISE_ID] = nextId();
            this._result = this._state = undefined;
            this._subscribers = [];
            if (noop !== resolver) {
                typeof resolver !== "function" && needsResolver();
                this instanceof Promise ? initializePromise(this, resolver) : needsNew();
            }
        }
        Promise.prototype.catch = function _catch(onRejection) {
            return this.then(null, onRejection);
        };
        Promise.prototype.finally = function _finally(callback) {
            var promise = this;
            var constructor = promise.constructor;
            return promise.then(function(value) {
                return constructor.resolve(callback()).then(function() {
                    return value;
                });
            }, function(reason) {
                return constructor.resolve(callback()).then(function() {
                    throw reason;
                });
            });
        };
        return Promise;
    }();
    Promise$2.prototype.then = then;
    Promise$2.all = all;
    Promise$2.race = race;
    Promise$2.resolve = resolve$1;
    Promise$2.reject = reject$1;
    Promise$2._setScheduler = setScheduler;
    Promise$2._setAsap = setAsap;
    Promise$2._asap = asap;
    function polyfill() {
        var local = void 0;
        if (typeof global !== "undefined") {
            local = global;
        } else if (typeof self !== "undefined") {
            local = self;
        } else {
            try {
                local = Function("return this")();
            } catch (e) {
                throw new Error("polyfill failed because global object is unavailable in this environment");
            }
        }
        var P = local.Promise;
        if (P) {
            var promiseToString = null;
            try {
                promiseToString = Object.prototype.toString.call(P.resolve());
            } catch (e) {}
            if (promiseToString === "[object Promise]" && !P.cast) {
                return;
            }
        }
        local.Promise = Promise$2;
    }
    Promise$2.polyfill = polyfill;
    Promise$2.Promise = Promise$2;
    Promise$2.polyfill();
    return Promise$2;
});

(function() {
    var Doc = "Document", doc = document, DOCUMENT = this[Doc] || this.HTMLDocument, WIN = "Window", win = window, WINDOW = this.constructor || this[WIN] || Window, HTMLELEMENT = "HTMLElement", documentElement = "documentElement", ELEMENT = Element, className = "className", add = "add", classList = "classList", remove = "remove", contains = "contains", CLASS = "class", setATTRIBUTE = "setAttribute", getATTRIBUTE = "getAttribute", prototype = "prototype", indexOf = "indexOf", length = "length", split = "split", trim = "trim", EVENT = "Event", CustomEvent = "CustomEvent", IE8EVENTS = "_events", etype = "type", target = "target", currentTarget = "currentTarget", relatedTarget = "relatedTarget", cancelable = "cancelable", bubbles = "bubbles", cancelBubble = "cancelBubble", cancelImmediate = "cancelImmediate", detail = "detail", addEventListener = "addEventListener", removeEventListener = "removeEventListener", dispatchEvent = "dispatchEvent";
    if (!win[HTMLELEMENT]) {
        win[HTMLELEMENT] = win[ELEMENT];
    }
    if (!Array[prototype][indexOf]) {
        Array[prototype][indexOf] = function(searchElement) {
            if (this === undefined || this === null) {
                throw new TypeError(this + " is not an object");
            }
            var arraylike = this instanceof String ? this[split]("") : this, lengthValue = Math.max(Math.min(arraylike[length], 9007199254740991), 0) || 0, index = Number(arguments[1]) || 0;
            index = (index < 0 ? Math.max(lengthValue + index, 0) : index) - 1;
            while (++index < lengthValue) {
                if (index in arraylike && arraylike[index] === searchElement) {
                    return index;
                }
            }
            return -1;
        };
    }
    if (!String[prototype][trim]) {
        String[prototype][trim] = function() {
            return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
        };
    }
    if (!(classList in ELEMENT[prototype])) {
        var ClassLIST = function(elem) {
            var classArr = (elem[getATTRIBUTE](CLASS) || "").replace(/^\s+|\s+$/g, "")[split](/\s+/) || [];
            hasClass = this[contains] = function(classNAME) {
                return classArr[indexOf](classNAME) > -1;
            }, addClass = this[add] = function(classNAME) {
                if (!hasClass(classNAME)) {
                    classArr.push(classNAME);
                    elem[setATTRIBUTE](CLASS, classArr.join(" "));
                }
            }, removeClass = this[remove] = function(classNAME) {
                if (hasClass(classNAME)) {
                    classArr.splice(classArr[indexOf](classNAME), 1);
                    elem[setATTRIBUTE](CLASS, classArr.join(" "));
                }
            }, toggleClass = this.toggle = function(classNAME) {
                if (hasClass(classNAME)) {
                    removeClass(classNAME);
                } else {
                    addClass(classNAME);
                }
            };
        };
        Object.defineProperty(ELEMENT[prototype], classList, {
            get: function() {
                return new ClassLIST(this);
            }
        });
    }
    if (!win[EVENT] || !WINDOW[prototype][EVENT]) {
        win[EVENT] = WINDOW[prototype][EVENT] = DOCUMENT[prototype][EVENT] = ELEMENT[prototype][EVENT] = function(type, eventInitDict) {
            if (!type) {
                throw new Error("Not enough arguments");
            }
            var event, bubblesValue = eventInitDict && eventInitDict[bubbles] !== undefined ? eventInitDict[bubbles] : false, cancelableValue = eventInitDict && eventInitDict[cancelable] !== undefined ? eventInitDict[cancelable] : false;
            if ("createEvent" in doc) {
                event = doc.createEvent(EVENT);
                event.initEvent(type, bubblesValue, cancelableValue);
            } else {
                event = doc.createEventObject();
                event[etype] = type;
                event[bubbles] = bubblesValue;
                event[cancelable] = cancelableValue;
            }
            return event;
        };
    }
    if (!(CustomEvent in win) || !(CustomEvent in WINDOW[prototype])) {
        win[CustomEvent] = WINDOW[prototype][CustomEvent] = DOCUMENT[prototype][CustomEvent] = Element[prototype][CustomEvent] = function(type, eventInitDict) {
            if (!type) {
                throw Error("CustomEvent TypeError: An event name must be provided.");
            }
            var event = new Event(type, eventInitDict);
            event[detail] = eventInitDict && eventInitDict[detail] || null;
            return event;
        };
    }
    if (!win[addEventListener] || !WINDOW[prototype][addEventListener]) {
        win[addEventListener] = WINDOW[prototype][addEventListener] = DOCUMENT[prototype][addEventListener] = ELEMENT[prototype][addEventListener] = function() {
            var element = this, type = arguments[0], listener = arguments[1];
            if (!element[IE8EVENTS]) {
                element[IE8EVENTS] = {};
            }
            if (!element[IE8EVENTS][type]) {
                element[IE8EVENTS][type] = function(event) {
                    var list = element[IE8EVENTS][event[etype]].list, events = list.slice(), index = -1, lengthValue = events[length], eventElement;
                    event.preventDefault = function() {
                        if (event[cancelable] !== false) {
                            event.returnValue = false;
                        }
                    };
                    event.stopPropagation = function() {
                        event[cancelBubble] = true;
                    };
                    event.stopImmediatePropagation = function() {
                        event[cancelBubble] = true;
                        event[cancelImmediate] = true;
                    };
                    event[currentTarget] = element;
                    event[relatedTarget] = event[relatedTarget] || event.fromElement || null;
                    event[target] = event[target] || event.srcElement || element;
                    event.timeStamp = new Date().getTime();
                    if (event.clientX) {
                        event.pageX = event.clientX + doc[documentElement].scrollLeft;
                        event.pageY = event.clientY + doc[documentElement].scrollTop;
                    }
                    while (++index < lengthValue && !event[cancelImmediate]) {
                        if (index in events) {
                            eventElement = events[index];
                            if (list[indexOf](eventElement) !== -1 && typeof eventElement === "function") {
                                eventElement.call(element, event);
                            }
                        }
                    }
                };
                element[IE8EVENTS][type].list = [];
                if (element.attachEvent) {
                    element.attachEvent("on" + type, element[IE8EVENTS][type]);
                }
            }
            element[IE8EVENTS][type].list.push(listener);
        };
        win[removeEventListener] = WINDOW[prototype][removeEventListener] = DOCUMENT[prototype][removeEventListener] = ELEMENT[prototype][removeEventListener] = function() {
            var element = this, type = arguments[0], listener = arguments[1], index;
            if (element[IE8EVENTS] && element[IE8EVENTS][type] && element[IE8EVENTS][type].list) {
                index = element[IE8EVENTS][type].list[indexOf](listener);
                if (index !== -1) {
                    element[IE8EVENTS][type].list.splice(index, 1);
                    if (!element[IE8EVENTS][type].list[length]) {
                        if (element.detachEvent) {
                            element.detachEvent("on" + type, element[IE8EVENTS][type]);
                        }
                        delete element[IE8EVENTS][type];
                    }
                }
            }
        };
    }
    if (!win[dispatchEvent] || !WINDOW[prototype][dispatchEvent] || !DOCUMENT[prototype][dispatchEvent] || !ELEMENT[prototype][dispatchEvent]) {
        win[dispatchEvent] = WINDOW[prototype][dispatchEvent] = DOCUMENT[prototype][dispatchEvent] = ELEMENT[prototype][dispatchEvent] = function(event) {
            if (!arguments[length]) {
                throw new Error("Not enough arguments");
            }
            if (!event || typeof event[etype] !== "string") {
                throw new Error("DOM Events Exception 0");
            }
            var element = this, type = event[etype];
            try {
                if (!event[bubbles]) {
                    event[cancelBubble] = true;
                    var cancelBubbleEvent = function(event) {
                        event[cancelBubble] = true;
                        (element || win).detachEvent("on" + type, cancelBubbleEvent);
                    };
                    this.attachEvent("on" + type, cancelBubbleEvent);
                }
                this.fireEvent("on" + type, event);
            } catch (error) {
                event[target] = element;
                do {
                    event[currentTarget] = element;
                    if (IE8EVENTS in element && typeof element[IE8EVENTS][type] === "function") {
                        element[IE8EVENTS][type].call(element, event);
                    }
                    if (typeof element["on" + type] === "function") {
                        element["on" + type].call(element, event);
                    }
                    element = element.nodeType === 9 ? element.parentWindow : element.parentNode;
                } while (element && !event[cancelBubble]);
            }
            return true;
        };
    }
})();

(function(root, factory) {
    if (typeof define === "function" && define.amd) {
        define([], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory();
    } else {
        var bsn = factory();
        root.Alert = bsn.Alert;
        root.Button = bsn.Button;
        root.Carousel = bsn.Carousel;
        root.Collapse = bsn.Collapse;
        root.Dropdown = bsn.Dropdown;
        root.Modal = bsn.Modal;
        root.Popover = bsn.Popover;
        root.ScrollSpy = bsn.ScrollSpy;
        root.Tab = bsn.Tab;
        root.Tooltip = bsn.Tooltip;
    }
})(this, function() {
    "use strict";
    var globalObject = typeof global !== "undefined" ? global : this || window, DOC = document, HTML = DOC.documentElement, body = "body", BSN = globalObject.BSN = {}, supports = BSN.supports = [], dataToggle = "data-toggle", dataDismiss = "data-dismiss", dataSpy = "data-spy", dataRide = "data-ride", stringAlert = "Alert", stringButton = "Button", stringCarousel = "Carousel", stringCollapse = "Collapse", stringDropdown = "Dropdown", stringModal = "Modal", stringPopover = "Popover", stringScrollSpy = "ScrollSpy", stringTab = "Tab", stringTooltip = "Tooltip", databackdrop = "data-backdrop", dataKeyboard = "data-keyboard", dataTarget = "data-target", dataInterval = "data-interval", dataHeight = "data-height", dataPause = "data-pause", dataTitle = "data-title", dataOriginalTitle = "data-original-title", dataOriginalText = "data-original-text", dataDismissible = "data-dismissible", dataTrigger = "data-trigger", dataAnimation = "data-animation", dataContainer = "data-container", dataPlacement = "data-placement", dataDelay = "data-delay", dataOffsetTop = "data-offset-top", dataOffsetBottom = "data-offset-bottom", backdrop = "backdrop", keyboard = "keyboard", delay = "delay", content = "content", target = "target", interval = "interval", pause = "pause", animation = "animation", placement = "placement", container = "container", offsetTop = "offsetTop", offsetBottom = "offsetBottom", offsetLeft = "offsetLeft", scrollTop = "scrollTop", scrollLeft = "scrollLeft", clientWidth = "clientWidth", clientHeight = "clientHeight", offsetWidth = "offsetWidth", offsetHeight = "offsetHeight", innerWidth = "innerWidth", innerHeight = "innerHeight", scrollHeight = "scrollHeight", height = "height", ariaExpanded = "aria-expanded", ariaHidden = "aria-hidden", clickEvent = "click", hoverEvent = "hover", keydownEvent = "keydown", keyupEvent = "keyup", resizeEvent = "resize", scrollEvent = "scroll", showEvent = "show", shownEvent = "shown", hideEvent = "hide", hiddenEvent = "hidden", closeEvent = "close", closedEvent = "closed", slidEvent = "slid", slideEvent = "slide", changeEvent = "change", getAttribute = "getAttribute", setAttribute = "setAttribute", hasAttribute = "hasAttribute", createElement = "createElement", appendChild = "appendChild", innerHTML = "innerHTML", getElementsByTagName = "getElementsByTagName", preventDefault = "preventDefault", getBoundingClientRect = "getBoundingClientRect", querySelectorAll = "querySelectorAll", getElementsByCLASSNAME = "getElementsByClassName", indexOf = "indexOf", parentNode = "parentNode", length = "length", toLowerCase = "toLowerCase", Transition = "Transition", Webkit = "Webkit", style = "style", push = "push", tabindex = "tabindex", contains = "contains", active = "active", showClass = "show", collapsing = "collapsing", disabled = "disabled", loading = "loading", left = "left", right = "right", top = "top", bottom = "bottom", mouseHover = "onmouseleave" in DOC ? [ "mouseenter", "mouseleave" ] : [ "mouseover", "mouseout" ], tipPositions = /\b(top|bottom|left|right)+/, modalOverlay = 0, fixedTop = "fixed-top", fixedBottom = "fixed-bottom", supportTransitions = Webkit + Transition in HTML[style] || Transition[toLowerCase]() in HTML[style], transitionEndEvent = Webkit + Transition in HTML[style] ? Webkit[toLowerCase]() + Transition + "End" : Transition[toLowerCase]() + "end", setFocus = function(element) {
        element.focus ? element.focus() : element.setActive();
    }, addClass = function(element, classNAME) {
        element.classList.add(classNAME);
    }, removeClass = function(element, classNAME) {
        element.classList.remove(classNAME);
    }, hasClass = function(element, classNAME) {
        return element.classList[contains](classNAME);
    }, getElementsByClassName = function(element, classNAME) {
        return [].slice.call(element[getElementsByCLASSNAME](classNAME));
    }, queryElement = function(selector, parent) {
        var lookUp = parent ? parent : DOC;
        return typeof selector === "object" ? selector : lookUp.querySelector(selector);
    }, getClosest = function(element, selector) {
        var firstChar = selector.charAt(0), selectorSubstring = selector.substr(1);
        if (firstChar === ".") {
            for (;element && element !== DOC; element = element[parentNode]) {
                if (queryElement(selector, element[parentNode]) !== null && hasClass(element, selectorSubstring)) {
                    return element;
                }
            }
        } else if (firstChar === "#") {
            for (;element && element !== DOC; element = element[parentNode]) {
                if (element.id === selectorSubstring) {
                    return element;
                }
            }
        }
        return false;
    }, on = function(element, event, handler) {
        element.addEventListener(event, handler, false);
    }, off = function(element, event, handler) {
        element.removeEventListener(event, handler, false);
    }, one = function(element, event, handler) {
        on(element, event, function handlerWrapper(e) {
            handler(e);
            off(element, event, handlerWrapper);
        });
    }, emulateTransitionEnd = function(element, handler) {
        if (supportTransitions) {
            one(element, transitionEndEvent, function(e) {
                handler(e);
            });
        } else {
            handler();
        }
    }, bootstrapCustomEvent = function(eventName, componentName, related) {
        var OriginalCustomEvent = new CustomEvent(eventName + ".bs." + componentName);
        OriginalCustomEvent.relatedTarget = related;
        this.dispatchEvent(OriginalCustomEvent);
    }, getScroll = function() {
        return {
            y: globalObject.pageYOffset || HTML[scrollTop],
            x: globalObject.pageXOffset || HTML[scrollLeft]
        };
    }, styleTip = function(link, element, position, parent) {
        var elementDimensions = {
            w: element[offsetWidth],
            h: element[offsetHeight]
        }, windowWidth = HTML[clientWidth] || DOC[body][clientWidth], windowHeight = HTML[clientHeight] || DOC[body][clientHeight], rect = link[getBoundingClientRect](), scroll = parent === DOC[body] ? getScroll() : {
            x: parent[offsetLeft] + parent[scrollLeft],
            y: parent[offsetTop] + parent[scrollTop]
        }, linkDimensions = {
            w: rect[right] - rect[left],
            h: rect[bottom] - rect[top]
        }, isPopover = hasClass(element, "popover"), topPosition, leftPosition, arrow = queryElement(".arrow", element), arrowTop, arrowLeft, arrowWidth, arrowHeight, halfTopExceed = rect[top] + linkDimensions.h / 2 - elementDimensions.h / 2 < 0, halfLeftExceed = rect[left] + linkDimensions.w / 2 - elementDimensions.w / 2 < 0, halfRightExceed = rect[left] + elementDimensions.w / 2 + linkDimensions.w / 2 >= windowWidth, halfBottomExceed = rect[top] + elementDimensions.h / 2 + linkDimensions.h / 2 >= windowHeight, topExceed = rect[top] - elementDimensions.h < 0, leftExceed = rect[left] - elementDimensions.w < 0, bottomExceed = rect[top] + elementDimensions.h + linkDimensions.h >= windowHeight, rightExceed = rect[left] + elementDimensions.w + linkDimensions.w >= windowWidth;
        position = (position === left || position === right) && leftExceed && rightExceed ? top : position;
        position = position === top && topExceed ? bottom : position;
        position = position === bottom && bottomExceed ? top : position;
        position = position === left && leftExceed ? right : position;
        position = position === right && rightExceed ? left : position;
        element.className[indexOf](position) === -1 && (element.className = element.className.replace(tipPositions, position));
        arrowWidth = arrow[offsetWidth];
        arrowHeight = arrow[offsetHeight];
        if (position === left || position === right) {
            if (position === left) {
                leftPosition = rect[left] + scroll.x - elementDimensions.w - (isPopover ? arrowWidth : 0);
            } else {
                leftPosition = rect[left] + scroll.x + linkDimensions.w;
            }
            if (halfTopExceed) {
                topPosition = rect[top] + scroll.y;
                arrowTop = linkDimensions.h / 2 - arrowWidth;
            } else if (halfBottomExceed) {
                topPosition = rect[top] + scroll.y - elementDimensions.h + linkDimensions.h;
                arrowTop = elementDimensions.h - linkDimensions.h / 2 - arrowWidth;
            } else {
                topPosition = rect[top] + scroll.y - elementDimensions.h / 2 + linkDimensions.h / 2;
                arrowTop = elementDimensions.h / 2 - (isPopover ? arrowHeight * .9 : arrowHeight / 2);
            }
        } else if (position === top || position === bottom) {
            if (position === top) {
                topPosition = rect[top] + scroll.y - elementDimensions.h - (isPopover ? arrowHeight : 0);
            } else {
                topPosition = rect[top] + scroll.y + linkDimensions.h;
            }
            if (halfLeftExceed) {
                leftPosition = 0;
                arrowLeft = rect[left] + linkDimensions.w / 2 - arrowWidth;
            } else if (halfRightExceed) {
                leftPosition = windowWidth - elementDimensions.w * 1.01;
                arrowLeft = elementDimensions.w - (windowWidth - rect[left]) + linkDimensions.w / 2 - arrowWidth / 2;
            } else {
                leftPosition = rect[left] + scroll.x - elementDimensions.w / 2 + linkDimensions.w / 2;
                arrowLeft = elementDimensions.w / 2 - arrowWidth / 2;
            }
        }
        element[style][top] = topPosition + "px";
        element[style][left] = leftPosition + "px";
        arrowTop && (arrow[style][top] = arrowTop + "px");
        arrowLeft && (arrow[style][left] = arrowLeft + "px");
    };
    BSN.version = "2.0.22";
    var Alert = function(element) {
        element = queryElement(element);
        var self = this, component = "alert", alert = getClosest(element, "." + component), triggerHandler = function() {
            hasClass(alert, "fade") ? emulateTransitionEnd(alert, transitionEndHandler) : transitionEndHandler();
        }, clickHandler = function(e) {
            alert = getClosest(e[target], "." + component);
            element = queryElement("[" + dataDismiss + '="' + component + '"]', alert);
            element && alert && (element === e[target] || element[contains](e[target])) && self.close();
        }, transitionEndHandler = function() {
            bootstrapCustomEvent.call(alert, closedEvent, component);
            off(element, clickEvent, clickHandler);
            alert[parentNode].removeChild(alert);
        };
        this.close = function() {
            if (alert && element && hasClass(alert, showClass)) {
                bootstrapCustomEvent.call(alert, closeEvent, component);
                removeClass(alert, showClass);
                alert && triggerHandler();
            }
        };
        if (!(stringAlert in element)) {
            on(element, clickEvent, clickHandler);
        }
        element[stringAlert] = self;
    };
    supports[push]([ stringAlert, Alert, "[" + dataDismiss + '="alert"]' ]);
    var Button = function(element) {
        element = queryElement(element);
        var toggled = false, component = "button", checked = "checked", reset = "reset", LABEL = "LABEL", INPUT = "INPUT", keyHandler = function(e) {
            var key = e.which || e.keyCode;
            key === 32 && e[target] === DOC.activeElement && toggle(e);
        }, preventScroll = function(e) {
            var key = e.which || e.keyCode;
            key === 32 && e[preventDefault]();
        }, toggle = function(e) {
            var label = e[target].tagName === LABEL ? e[target] : e[target][parentNode].tagName === LABEL ? e[target][parentNode] : null;
            if (!label) return;
            var eventTarget = e[target], labels = getElementsByClassName(eventTarget[parentNode], "btn"), input = label[getElementsByTagName](INPUT)[0];
            if (!input) return;
            if (input.type === "checkbox") {
                if (!input[checked]) {
                    addClass(label, active);
                    input[getAttribute](checked);
                    input[setAttribute](checked, checked);
                    input[checked] = true;
                } else {
                    removeClass(label, active);
                    input[getAttribute](checked);
                    input.removeAttribute(checked);
                    input[checked] = false;
                }
                if (!toggled) {
                    toggled = true;
                    bootstrapCustomEvent.call(input, changeEvent, component);
                    bootstrapCustomEvent.call(element, changeEvent, component);
                }
            }
            if (input.type === "radio" && !toggled) {
                if (!input[checked]) {
                    addClass(label, active);
                    input[setAttribute](checked, checked);
                    input[checked] = true;
                    bootstrapCustomEvent.call(input, changeEvent, component);
                    bootstrapCustomEvent.call(element, changeEvent, component);
                    toggled = true;
                    for (var i = 0, ll = labels[length]; i < ll; i++) {
                        var otherLabel = labels[i], otherInput = otherLabel[getElementsByTagName](INPUT)[0];
                        if (otherLabel !== label && hasClass(otherLabel, active)) {
                            removeClass(otherLabel, active);
                            otherInput.removeAttribute(checked);
                            otherInput[checked] = false;
                            bootstrapCustomEvent.call(otherInput, changeEvent, component);
                        }
                    }
                }
            }
            setTimeout(function() {
                toggled = false;
            }, 50);
        };
        if (!(stringButton in element)) {
            on(element, clickEvent, toggle);
            queryElement("[" + tabindex + "]", element) && on(element, keyupEvent, keyHandler), 
            on(element, keydownEvent, preventScroll);
        }
        var labelsToACtivate = getElementsByClassName(element, "btn"), lbll = labelsToACtivate[length];
        for (var i = 0; i < lbll; i++) {
            !hasClass(labelsToACtivate[i], active) && queryElement("input:checked", labelsToACtivate[i]) && addClass(labelsToACtivate[i], active);
        }
        element[stringButton] = this;
    };
    supports[push]([ stringButton, Button, "[" + dataToggle + '="buttons"]' ]);
    var Carousel = function(element, options) {
        element = queryElement(element);
        options = options || {};
        var intervalAttribute = element[getAttribute](dataInterval), intervalOption = options[interval], intervalData = intervalAttribute === "false" ? 0 : parseInt(intervalAttribute) || 5e3, pauseData = element[getAttribute](dataPause) === hoverEvent || false, keyboardData = element[getAttribute](dataKeyboard) === "true" || false, component = "carousel", paused = "paused", direction = "direction", carouselItem = "carousel-item", dataSlideTo = "data-slide-to";
        this[keyboard] = options[keyboard] === true || keyboardData;
        this[pause] = options[pause] === hoverEvent || pauseData ? hoverEvent : false;
        this[interval] = typeof intervalOption === "number" ? intervalOption : intervalData === 0 ? 0 : intervalData;
        var self = this, index = element.index = 0, timer = element.timer = 0, isSliding = false, slides = getElementsByClassName(element, carouselItem), total = slides[length], slideDirection = this[direction] = left, leftArrow = getElementsByClassName(element, component + "-control-prev")[0], rightArrow = getElementsByClassName(element, component + "-control-next")[0], indicator = queryElement("." + component + "-indicators", element), indicators = indicator && indicator[getElementsByTagName]("LI") || [];
        var pauseHandler = function() {
            if (self[interval] !== false && !hasClass(element, paused)) {
                addClass(element, paused);
                !isSliding && clearInterval(timer);
            }
        }, resumeHandler = function() {
            if (self[interval] !== false && hasClass(element, paused)) {
                removeClass(element, paused);
                !isSliding && clearInterval(timer);
                !isSliding && self.cycle();
            }
        }, indicatorHandler = function(e) {
            e[preventDefault]();
            if (isSliding) return;
            var eventTarget = e[target];
            if (eventTarget && !hasClass(eventTarget, active) && eventTarget[getAttribute](dataSlideTo)) {
                index = parseInt(eventTarget[getAttribute](dataSlideTo), 10);
            } else {
                return false;
            }
            self.slideTo(index);
        }, controlsHandler = function(e) {
            e[preventDefault]();
            if (isSliding) return;
            var eventTarget = e.currentTarget || e.srcElement;
            if (eventTarget === rightArrow) {
                index++;
            } else if (eventTarget === leftArrow) {
                index--;
            }
            self.slideTo(index);
        }, keyHandler = function(e) {
            if (isSliding) return;
            switch (e.which) {
              case 39:
                index++;
                break;

              case 37:
                index--;
                break;

              default:
                return;
            }
            self.slideTo(index);
        }, isElementInScrollRange = function() {
            var rect = element[getBoundingClientRect](), viewportHeight = globalObject[innerHeight] || HTML[clientHeight];
            return rect[top] <= viewportHeight && rect[bottom] >= 0;
        }, setActivePage = function(pageIndex) {
            for (var i = 0, icl = indicators[length]; i < icl; i++) {
                removeClass(indicators[i], active);
            }
            if (indicators[pageIndex]) addClass(indicators[pageIndex], active);
        };
        this.cycle = function() {
            timer = setInterval(function() {
                isElementInScrollRange() && (index++, self.slideTo(index));
            }, this[interval]);
        };
        this.slideTo = function(next) {
            if (isSliding) return;
            var activeItem = this.getActiveIndex(), orientation;
            if (activeItem < next || activeItem === 0 && next === total - 1) {
                slideDirection = self[direction] = left;
            } else if (activeItem > next || activeItem === total - 1 && next === 0) {
                slideDirection = self[direction] = right;
            }
            if (next < 0) {
                next = total - 1;
            } else if (next === total) {
                next = 0;
            }
            index = next;
            orientation = slideDirection === left ? "next" : "prev";
            bootstrapCustomEvent.call(element, slideEvent, component, slides[next]);
            isSliding = true;
            clearInterval(timer);
            setActivePage(next);
            if (supportTransitions && hasClass(element, "slide")) {
                addClass(slides[next], carouselItem + "-" + orientation);
                slides[next][offsetWidth];
                addClass(slides[next], carouselItem + "-" + slideDirection);
                addClass(slides[activeItem], carouselItem + "-" + slideDirection);
                one(slides[activeItem], transitionEndEvent, function(e) {
                    var timeout = e[target] !== slides[activeItem] ? e.elapsedTime * 1e3 : 0;
                    setTimeout(function() {
                        isSliding = false;
                        addClass(slides[next], active);
                        removeClass(slides[activeItem], active);
                        removeClass(slides[next], carouselItem + "-" + orientation);
                        removeClass(slides[next], carouselItem + "-" + slideDirection);
                        removeClass(slides[activeItem], carouselItem + "-" + slideDirection);
                        bootstrapCustomEvent.call(element, slidEvent, component, slides[next]);
                        if (!DOC.hidden && self[interval] && !hasClass(element, paused)) {
                            self.cycle();
                        }
                    }, timeout + 100);
                });
            } else {
                addClass(slides[next], active);
                slides[next][offsetWidth];
                removeClass(slides[activeItem], active);
                setTimeout(function() {
                    isSliding = false;
                    if (self[interval] && !hasClass(element, paused)) {
                        self.cycle();
                    }
                    bootstrapCustomEvent.call(element, slidEvent, component, slides[next]);
                }, 100);
            }
        };
        this.getActiveIndex = function() {
            return slides[indexOf](getElementsByClassName(element, carouselItem + " active")[0]) || 0;
        };
        if (!(stringCarousel in element)) {
            if (self[pause] && self[interval]) {
                on(element, mouseHover[0], pauseHandler);
                on(element, mouseHover[1], resumeHandler);
                on(element, "touchstart", pauseHandler);
                on(element, "touchend", resumeHandler);
            }
            rightArrow && on(rightArrow, clickEvent, controlsHandler);
            leftArrow && on(leftArrow, clickEvent, controlsHandler);
            indicator && on(indicator, clickEvent, indicatorHandler);
            self[keyboard] === true && on(globalObject, keydownEvent, keyHandler);
        }
        if (self.getActiveIndex() < 0) {
            slides[length] && addClass(slides[0], active);
            indicators[length] && setActivePage(0);
        }
        if (self[interval]) {
            self.cycle();
        }
        element[stringCarousel] = self;
    };
    supports[push]([ stringCarousel, Carousel, "[" + dataRide + '="carousel"]' ]);
    var Collapse = function(element, options) {
        element = queryElement(element);
        options = options || {};
        var accordion = null, collapse = null, self = this, isAnimating = false, accordionData = element[getAttribute]("data-parent"), component = "collapse", collapsed = "collapsed", openAction = function(collapseElement, toggle) {
            bootstrapCustomEvent.call(collapseElement, showEvent, component);
            isAnimating = true;
            addClass(collapseElement, collapsing);
            removeClass(collapseElement, component);
            collapseElement[style][height] = collapseElement[scrollHeight] + "px";
            emulateTransitionEnd(collapseElement, function() {
                isAnimating = false;
                collapseElement[setAttribute](ariaExpanded, "true");
                toggle[setAttribute](ariaExpanded, "true");
                removeClass(collapseElement, collapsing);
                addClass(collapseElement, component);
                addClass(collapseElement, showClass);
                collapseElement[style][height] = "";
                bootstrapCustomEvent.call(collapseElement, shownEvent, component);
            });
        }, closeAction = function(collapseElement, toggle) {
            bootstrapCustomEvent.call(collapseElement, hideEvent, component);
            isAnimating = true;
            collapseElement[style][height] = collapseElement[scrollHeight] + "px";
            removeClass(collapseElement, component);
            removeClass(collapseElement, showClass);
            addClass(collapseElement, collapsing);
            collapseElement[offsetWidth];
            collapseElement[style][height] = "0px";
            emulateTransitionEnd(collapseElement, function() {
                isAnimating = false;
                collapseElement[setAttribute](ariaExpanded, "false");
                toggle[setAttribute](ariaExpanded, "false");
                removeClass(collapseElement, collapsing);
                addClass(collapseElement, component);
                collapseElement[style][height] = "";
                bootstrapCustomEvent.call(collapseElement, hiddenEvent, component);
            });
        }, getTarget = function() {
            var href = element.href && element[getAttribute]("href"), parent = element[getAttribute](dataTarget), id = href || parent && parent.charAt(0) === "#" && parent;
            return id && queryElement(id);
        };
        this.toggle = function(e) {
            e[preventDefault]();
            if (isAnimating) return;
            if (!hasClass(collapse, showClass)) {
                self.show();
            } else {
                self.hide();
            }
        };
        this.hide = function() {
            closeAction(collapse, element);
            addClass(element, collapsed);
        };
        this.show = function() {
            if (accordion) {
                var activeCollapse = queryElement("." + component + "." + showClass, accordion), toggle = activeCollapse && (queryElement("[" + dataToggle + '="' + component + '"][' + dataTarget + '="#' + activeCollapse.id + '"]', accordion) || queryElement("[" + dataToggle + '="' + component + '"][href="#' + activeCollapse.id + '"]', accordion)), correspondingCollapse = toggle && (toggle[getAttribute](dataTarget) || toggle.href);
                if (activeCollapse && toggle && activeCollapse !== collapse) {
                    closeAction(activeCollapse, toggle);
                    if (correspondingCollapse.split("#")[1] !== collapse.id) {
                        addClass(toggle, collapsed);
                    } else {
                        removeClass(toggle, collapsed);
                    }
                }
            }
            openAction(collapse, element);
            removeClass(element, collapsed);
        };
        if (!(stringCollapse in element)) {
            on(element, clickEvent, self.toggle);
        }
        collapse = getTarget();
        accordion = queryElement(options.parent) || accordionData && getClosest(element, accordionData);
        element[stringCollapse] = self;
    };
    supports[push]([ stringCollapse, Collapse, "[" + dataToggle + '="collapse"]' ]);
    var Dropdown = function(element, option) {
        element = queryElement(element);
        this.persist = option === true || element[getAttribute]("data-persist") === "true" || false;
        var self = this, children = "children", parent = element[parentNode], component = "dropdown", open = "open", relatedTarget = null, menu = queryElement(".dropdown-menu", parent), menuItems = function() {
            var set = menu[children], newSet = [];
            for (var i = 0; i < set[length]; i++) {
                set[i][children][length] && (set[i][children][0].tagName === "A" && newSet[push](set[i][children][0]));
                set[i].tagName === "A" && newSet[push](set[i]);
            }
            return newSet;
        }(), preventEmptyAnchor = function(anchor) {
            (anchor.href && anchor.href.slice(-1) === "#" || anchor[parentNode] && anchor[parentNode].href && anchor[parentNode].href.slice(-1) === "#") && this[preventDefault]();
        }, toggleDismiss = function() {
            var type = element[open] ? on : off;
            type(DOC, clickEvent, dismissHandler);
            type(DOC, keydownEvent, preventScroll);
            type(DOC, keyupEvent, keyHandler);
        }, dismissHandler = function(e) {
            var eventTarget = e[target], hasData = eventTarget && (stringDropdown in eventTarget || stringDropdown in eventTarget[parentNode]);
            if ((eventTarget === menu || menu[contains](eventTarget)) && (self.persist || hasData)) {
                return;
            } else {
                relatedTarget = eventTarget === element || element[contains](eventTarget) ? element : null;
                hide();
            }
            preventEmptyAnchor.call(e, eventTarget);
        }, clickHandler = function(e) {
            relatedTarget = element;
            show();
            preventEmptyAnchor.call(e, e[target]);
        }, preventScroll = function(e) {
            var key = e.which || e.keyCode;
            if (key === 38 || key === 40) {
                e[preventDefault]();
            }
        }, keyHandler = function(e) {
            var key = e.which || e.keyCode, activeItem = DOC.activeElement, idx = menuItems[indexOf](activeItem), isSameElement = activeItem === element, isInsideMenu = menu[contains](activeItem), isMenuItem = activeItem[parentNode] === menu || activeItem[parentNode][parentNode] === menu;
            if (isMenuItem || isSameElement) {
                idx = isSameElement ? 0 : key === 38 ? idx > 1 ? idx - 1 : 0 : key === 40 ? idx < menuItems[length] - 1 ? idx + 1 : idx : idx;
                menuItems[idx] && setFocus(menuItems[idx]);
            }
            if ((menuItems[length] && isMenuItem || !menuItems[length] && (isInsideMenu || isSameElement) || !isInsideMenu) && element[open] && key === 27) {
                self.toggle();
                relatedTarget = null;
            }
        }, show = function() {
            bootstrapCustomEvent.call(parent, showEvent, component, relatedTarget);
            addClass(menu, showClass);
            addClass(parent, showClass);
            menu[setAttribute](ariaExpanded, true);
            bootstrapCustomEvent.call(parent, shownEvent, component, relatedTarget);
            element[open] = true;
            off(element, clickEvent, clickHandler);
            setTimeout(function() {
                setFocus(menu[getElementsByTagName]("INPUT")[0] || element);
                toggleDismiss();
            }, 1);
        }, hide = function() {
            bootstrapCustomEvent.call(parent, hideEvent, component, relatedTarget);
            removeClass(menu, showClass);
            removeClass(parent, showClass);
            menu[setAttribute](ariaExpanded, false);
            bootstrapCustomEvent.call(parent, hiddenEvent, component, relatedTarget);
            element[open] = false;
            toggleDismiss();
            setFocus(element);
            setTimeout(function() {
                on(element, clickEvent, clickHandler);
            }, 1);
        };
        element[open] = false;
        this.toggle = function() {
            if (hasClass(parent, showClass) && element[open]) {
                hide();
            } else {
                show();
            }
        };
        if (!(stringDropdown in element)) {
            !tabindex in menu && menu[setAttribute](tabindex, "0");
            on(element, clickEvent, clickHandler);
        }
        element[stringDropdown] = self;
    };
    supports[push]([ stringDropdown, Dropdown, "[" + dataToggle + '="dropdown"]' ]);
    var Modal = function(element, options) {
        element = queryElement(element);
        var btnCheck = element[getAttribute](dataTarget) || element[getAttribute]("href"), checkModal = queryElement(btnCheck), modal = hasClass(element, "modal") ? element : checkModal, component = "modal", staticString = "static", paddingLeft = "paddingLeft", paddingRight = "paddingRight", modalBackdropString = "modal-backdrop";
        if (hasClass(element, "modal")) {
            element = null;
        }
        if (!modal) {
            return;
        }
        options = options || {};
        this[keyboard] = options[keyboard] === false || modal[getAttribute](dataKeyboard) === "false" ? false : true;
        this[backdrop] = options[backdrop] === staticString || modal[getAttribute](databackdrop) === staticString ? staticString : true;
        this[backdrop] = options[backdrop] === false || modal[getAttribute](databackdrop) === "false" ? false : this[backdrop];
        this[content] = options[content];
        var self = this, relatedTarget = null, bodyIsOverflowing, modalIsOverflowing, scrollbarWidth, overlay, fixedItems = getElementsByClassName(HTML, fixedTop).concat(getElementsByClassName(HTML, fixedBottom)), getWindowWidth = function() {
            var htmlRect = HTML[getBoundingClientRect]();
            return globalObject[innerWidth] || htmlRect[right] - Math.abs(htmlRect[left]);
        }, setScrollbar = function() {
            var bodyStyle = globalObject.getComputedStyle(DOC[body]), bodyPad = parseInt(bodyStyle[paddingRight], 10), itemPad;
            if (bodyIsOverflowing) {
                DOC[body][style][paddingRight] = bodyPad + scrollbarWidth + "px";
                if (fixedItems[length]) {
                    for (var i = 0; i < fixedItems[length]; i++) {
                        itemPad = globalObject.getComputedStyle(fixedItems[i])[paddingRight];
                        fixedItems[i][style][paddingRight] = parseInt(itemPad) + scrollbarWidth + "px";
                    }
                }
            }
        }, resetScrollbar = function() {
            DOC[body][style][paddingRight] = "";
            if (fixedItems[length]) {
                for (var i = 0; i < fixedItems[length]; i++) {
                    fixedItems[i][style][paddingRight] = "";
                }
            }
        }, measureScrollbar = function() {
            var scrollDiv = DOC[createElement]("div"), scrollBarWidth;
            scrollDiv.className = component + "-scrollbar-measure";
            DOC[body][appendChild](scrollDiv);
            scrollBarWidth = scrollDiv[offsetWidth] - scrollDiv[clientWidth];
            DOC[body].removeChild(scrollDiv);
            return scrollBarWidth;
        }, checkScrollbar = function() {
            bodyIsOverflowing = DOC[body][clientWidth] < getWindowWidth();
            modalIsOverflowing = modal[scrollHeight] > HTML[clientHeight];
            scrollbarWidth = measureScrollbar();
        }, adjustDialog = function() {
            modal[style][paddingLeft] = !bodyIsOverflowing && modalIsOverflowing ? scrollbarWidth + "px" : "";
            modal[style][paddingRight] = bodyIsOverflowing && !modalIsOverflowing ? scrollbarWidth + "px" : "";
        }, resetAdjustments = function() {
            modal[style][paddingLeft] = "";
            modal[style][paddingRight] = "";
        }, createOverlay = function() {
            modalOverlay = 1;
            var newOverlay = DOC[createElement]("div");
            overlay = queryElement("." + modalBackdropString);
            if (overlay === null) {
                newOverlay[setAttribute]("class", modalBackdropString + " fade");
                overlay = newOverlay;
                DOC[body][appendChild](overlay);
            }
        }, removeOverlay = function() {
            overlay = queryElement("." + modalBackdropString);
            if (overlay && overlay !== null && typeof overlay === "object") {
                modalOverlay = 0;
                DOC[body].removeChild(overlay);
                overlay = null;
            }
            bootstrapCustomEvent.call(modal, hiddenEvent, component);
        }, keydownHandlerToggle = function() {
            if (hasClass(modal, showClass)) {
                on(DOC, keydownEvent, keyHandler);
            } else {
                off(DOC, keydownEvent, keyHandler);
            }
        }, resizeHandlerToggle = function() {
            if (hasClass(modal, showClass)) {
                on(globalObject, resizeEvent, self.update);
            } else {
                off(globalObject, resizeEvent, self.update);
            }
        }, dismissHandlerToggle = function() {
            if (hasClass(modal, showClass)) {
                on(modal, clickEvent, dismissHandler);
            } else {
                off(modal, clickEvent, dismissHandler);
            }
        }, triggerShow = function() {
            setFocus(modal);
            bootstrapCustomEvent.call(modal, shownEvent, component, relatedTarget);
        }, triggerHide = function() {
            modal[style].display = "";
            element && setFocus(element);
            (function() {
                if (!getElementsByClassName(DOC, component + " " + showClass)[0]) {
                    resetAdjustments();
                    resetScrollbar();
                    removeClass(DOC[body], component + "-open");
                    overlay && hasClass(overlay, "fade") ? (removeClass(overlay, showClass), emulateTransitionEnd(overlay, removeOverlay)) : removeOverlay();
                    resizeHandlerToggle();
                    dismissHandlerToggle();
                    keydownHandlerToggle();
                }
            })();
        }, clickHandler = function(e) {
            var clickTarget = e[target];
            clickTarget = clickTarget[hasAttribute](dataTarget) || clickTarget[hasAttribute]("href") ? clickTarget : clickTarget[parentNode];
            if (clickTarget === element && !hasClass(modal, showClass)) {
                modal.modalTrigger = element;
                relatedTarget = element;
                self.show();
                e[preventDefault]();
            }
        }, keyHandler = function(e) {
            if (self[keyboard] && e.which == 27 && hasClass(modal, showClass)) {
                self.hide();
            }
        }, dismissHandler = function(e) {
            var clickTarget = e[target];
            if (hasClass(modal, showClass) && (clickTarget[parentNode][getAttribute](dataDismiss) === component || clickTarget[getAttribute](dataDismiss) === component || clickTarget === modal && self[backdrop] !== staticString)) {
                self.hide();
                relatedTarget = null;
                e[preventDefault]();
            }
        };
        this.toggle = function() {
            if (hasClass(modal, showClass)) {
                this.hide();
            } else {
                this.show();
            }
        };
        this.show = function() {
            bootstrapCustomEvent.call(modal, showEvent, component, relatedTarget);
            var currentOpen = getElementsByClassName(DOC, component + " " + showClass)[0];
            currentOpen && currentOpen !== modal && currentOpen.modalTrigger[stringModal].hide();
            if (this[backdrop]) {
                !modalOverlay && createOverlay();
            }
            if (overlay && modalOverlay && !hasClass(overlay, showClass)) {
                overlay[offsetWidth];
                addClass(overlay, showClass);
            }
            setTimeout(function() {
                modal[style].display = "block";
                checkScrollbar();
                setScrollbar();
                adjustDialog();
                addClass(DOC[body], component + "-open");
                addClass(modal, showClass);
                modal[setAttribute](ariaHidden, false);
                resizeHandlerToggle();
                dismissHandlerToggle();
                keydownHandlerToggle();
                hasClass(modal, "fade") ? emulateTransitionEnd(modal, triggerShow) : triggerShow();
            }, supportTransitions ? 150 : 0);
        };
        this.hide = function() {
            bootstrapCustomEvent.call(modal, hideEvent, component);
            overlay = queryElement("." + modalBackdropString);
            removeClass(modal, showClass);
            modal[setAttribute](ariaHidden, true);
            (function() {
                hasClass(modal, "fade") ? emulateTransitionEnd(modal, triggerHide) : triggerHide();
            })();
        };
        this.setContent = function(content) {
            queryElement("." + component + "-content", modal)[innerHTML] = content;
        };
        this.update = function() {
            if (hasClass(modal, showClass)) {
                checkScrollbar();
                setScrollbar();
                adjustDialog();
            }
        };
        if (!!element && !(stringModal in element)) {
            on(element, clickEvent, clickHandler);
        }
        if (!!self[content]) {
            self.setContent(self[content]);
        }
        !!element && (element[stringModal] = self);
    };
    supports[push]([ stringModal, Modal, "[" + dataToggle + '="modal"]' ]);
    var Popover = function(element, options) {
        element = queryElement(element);
        options = options || {};
        var triggerData = element[getAttribute](dataTrigger), animationData = element[getAttribute](dataAnimation), placementData = element[getAttribute](dataPlacement), dismissibleData = element[getAttribute](dataDismissible), delayData = element[getAttribute](dataDelay), containerData = element[getAttribute](dataContainer), component = "popover", template = "template", trigger = "trigger", classString = "class", div = "div", fade = "fade", content = "content", dataContent = "data-content", dismissible = "dismissible", closeBtn = '<button type="button" class="close">×</button>', containerElement = queryElement(options[container]), containerDataElement = queryElement(containerData), modal = getClosest(element, ".modal"), navbarFixedTop = getClosest(element, "." + fixedTop), navbarFixedBottom = getClosest(element, "." + fixedBottom);
        this[template] = options[template] ? options[template] : null;
        this[trigger] = options[trigger] ? options[trigger] : triggerData || hoverEvent;
        this[animation] = options[animation] && options[animation] !== fade ? options[animation] : animationData || fade;
        this[placement] = options[placement] ? options[placement] : placementData || top;
        this[delay] = parseInt(options[delay] || delayData) || 200;
        this[dismissible] = options[dismissible] || dismissibleData === "true" ? true : false;
        this[container] = containerElement ? containerElement : containerDataElement ? containerDataElement : navbarFixedTop ? navbarFixedTop : navbarFixedBottom ? navbarFixedBottom : modal ? modal : DOC[body];
        var self = this, titleString = element[getAttribute](dataTitle) || null, contentString = element[getAttribute](dataContent) || null;
        if (!contentString && !this[template]) return;
        var popover = null, timer = 0, placementSetting = this[placement], dismissibleHandler = function(e) {
            if (popover !== null && e[target] === queryElement(".close", popover)) {
                self.hide();
            }
        }, removePopover = function() {
            self[container].removeChild(popover);
            timer = null;
            popover = null;
        }, createPopover = function() {
            titleString = element[getAttribute](dataTitle);
            contentString = element[getAttribute](dataContent);
            popover = DOC[createElement](div);
            var popoverArrow = DOC[createElement](div);
            popoverArrow[setAttribute](classString, "arrow");
            popover[appendChild](popoverArrow);
            if (contentString !== null && self[template] === null) {
                popover[setAttribute]("role", "tooltip");
                if (titleString !== null) {
                    var popoverTitle = DOC[createElement]("h3");
                    popoverTitle[setAttribute](classString, component + "-header");
                    popoverTitle[innerHTML] = self[dismissible] ? titleString + closeBtn : titleString;
                    popover[appendChild](popoverTitle);
                }
                var popoverContent = DOC[createElement](div);
                popoverContent[setAttribute](classString, component + "-body");
                popoverContent[innerHTML] = self[dismissible] && titleString === null ? contentString + closeBtn : contentString;
                popover[appendChild](popoverContent);
            } else {
                var popoverTemplate = DOC[createElement](div);
                popoverTemplate[innerHTML] = self[template];
                popover[innerHTML] = popoverTemplate.firstChild[innerHTML];
            }
            self[container][appendChild](popover);
            popover[style].display = "block";
            popover[setAttribute](classString, component + " bs-" + component + "-" + placementSetting + " " + self[animation]);
        }, showPopover = function() {
            !hasClass(popover, showClass) && addClass(popover, showClass);
        }, updatePopover = function() {
            styleTip(element, popover, placementSetting, self[container]);
        }, dismissHandlerToggle = function(type) {
            if (clickEvent == self[trigger] || "focus" == self[trigger]) {
                !self[dismissible] && type(element, "blur", self.hide);
            }
            self[dismissible] && type(DOC, clickEvent, dismissibleHandler);
            type(globalObject, resizeEvent, self.hide);
        }, showTrigger = function() {
            dismissHandlerToggle(on);
            bootstrapCustomEvent.call(element, shownEvent, component);
        }, hideTrigger = function() {
            dismissHandlerToggle(off);
            removePopover();
            bootstrapCustomEvent.call(element, hiddenEvent, component);
        };
        this.toggle = function() {
            if (popover === null) {
                self.show();
            } else {
                self.hide();
            }
        };
        this.show = function() {
            clearTimeout(timer);
            timer = setTimeout(function() {
                if (popover === null) {
                    placementSetting = self[placement];
                    createPopover();
                    updatePopover();
                    showPopover();
                    bootstrapCustomEvent.call(element, showEvent, component);
                    !!self[animation] ? emulateTransitionEnd(popover, showTrigger) : showTrigger();
                }
            }, 20);
        };
        this.hide = function() {
            clearTimeout(timer);
            timer = setTimeout(function() {
                if (popover && popover !== null && hasClass(popover, showClass)) {
                    bootstrapCustomEvent.call(element, hideEvent, component);
                    removeClass(popover, showClass);
                    !!self[animation] ? emulateTransitionEnd(popover, hideTrigger) : hideTrigger();
                }
            }, self[delay]);
        };
        if (!(stringPopover in element)) {
            if (self[trigger] === hoverEvent) {
                on(element, mouseHover[0], self.show);
                if (!self[dismissible]) {
                    on(element, mouseHover[1], self.hide);
                }
            } else if (clickEvent == self[trigger] || "focus" == self[trigger]) {
                on(element, self[trigger], self.toggle);
            }
        }
        element[stringPopover] = self;
    };
    supports[push]([ stringPopover, Popover, "[" + dataToggle + '="popover"]' ]);
    var ScrollSpy = function(element, options) {
        element = queryElement(element);
        var targetData = queryElement(element[getAttribute](dataTarget)), offsetData = element[getAttribute]("data-offset");
        options = options || {};
        if (!options[target] && !targetData) {
            return;
        }
        var self = this, spyTarget = options[target] && queryElement(options[target]) || targetData, links = spyTarget && spyTarget[getElementsByTagName]("A"), offset = parseInt(offsetData || options["offset"]) || 10, items = [], targetItems = [], scrollOffset, scrollTarget = element[offsetHeight] < element[scrollHeight] ? element : globalObject, isWindow = scrollTarget === globalObject;
        for (var i = 0, il = links[length]; i < il; i++) {
            var href = links[i][getAttribute]("href"), targetItem = href && href.charAt(0) === "#" && href.slice(-1) !== "#" && queryElement(href);
            if (!!targetItem) {
                items[push](links[i]);
                targetItems[push](targetItem);
            }
        }
        var updateItem = function(index) {
            var item = items[index], targetItem = targetItems[index], dropdown = item[parentNode][parentNode], dropdownLink = hasClass(dropdown, "dropdown") && dropdown[getElementsByTagName]("A")[0], targetRect = isWindow && targetItem[getBoundingClientRect](), isActive = hasClass(item, active) || false, topEdge = (isWindow ? targetRect[top] + scrollOffset : targetItem[offsetTop]) - offset, bottomEdge = isWindow ? targetRect[bottom] + scrollOffset - offset : targetItems[index + 1] ? targetItems[index + 1][offsetTop] - offset : element[scrollHeight], inside = scrollOffset >= topEdge && bottomEdge > scrollOffset;
            if (!isActive && inside) {
                if (!hasClass(item, active)) {
                    addClass(item, active);
                    if (dropdownLink && !hasClass(dropdownLink, active)) {
                        addClass(dropdownLink, active);
                    }
                    bootstrapCustomEvent.call(element, "activate", "scrollspy", items[index]);
                }
            } else if (!inside) {
                if (hasClass(item, active)) {
                    removeClass(item, active);
                    if (dropdownLink && hasClass(dropdownLink, active) && !getElementsByClassName(item[parentNode], active).length) {
                        removeClass(dropdownLink, active);
                    }
                }
            } else if (!inside && !isActive || isActive && inside) {
                return;
            }
        }, updateItems = function() {
            scrollOffset = isWindow ? getScroll().y : element[scrollTop];
            for (var index = 0, itl = items[length]; index < itl; index++) {
                updateItem(index);
            }
        };
        this.refresh = function() {
            updateItems();
        };
        if (!(stringScrollSpy in element)) {
            on(scrollTarget, scrollEvent, self.refresh);
            on(globalObject, resizeEvent, self.refresh);
        }
        self.refresh();
        element[stringScrollSpy] = self;
    };
    supports[push]([ stringScrollSpy, ScrollSpy, "[" + dataSpy + '="scroll"]' ]);
    var Tab = function(element, options) {
        element = queryElement(element);
        var heightData = element[getAttribute](dataHeight), component = "tab", height = "height", float = "float", isAnimating = "isAnimating";
        options = options || {};
        this[height] = supportTransitions ? options[height] || heightData === "true" : false;
        var self = this, next, tabs = getClosest(element, ".nav"), tabsContentContainer = false, dropdown = tabs && queryElement(".dropdown-toggle", tabs), activeTab, activeContent, nextContent, containerHeight, equalContents, nextHeight, triggerEnd = function() {
            tabsContentContainer[style][height] = "";
            removeClass(tabsContentContainer, collapsing);
            tabs[isAnimating] = false;
        }, triggerShow = function() {
            if (tabsContentContainer) {
                if (equalContents) {
                    triggerEnd();
                } else {
                    setTimeout(function() {
                        tabsContentContainer[style][height] = nextHeight + "px";
                        tabsContentContainer[offsetWidth];
                        emulateTransitionEnd(tabsContentContainer, triggerEnd);
                    }, 1);
                }
            } else {
                tabs[isAnimating] = false;
            }
            bootstrapCustomEvent.call(next, shownEvent, component, activeTab);
        }, triggerHide = function() {
            if (tabsContentContainer) {
                activeContent[style][float] = left;
                nextContent[style][float] = left;
                containerHeight = activeContent[scrollHeight];
            }
            addClass(nextContent, active);
            bootstrapCustomEvent.call(next, showEvent, component, activeTab);
            removeClass(activeContent, active);
            bootstrapCustomEvent.call(activeTab, hiddenEvent, component, next);
            if (tabsContentContainer) {
                nextHeight = nextContent[scrollHeight];
                equalContents = nextHeight === containerHeight;
                addClass(tabsContentContainer, collapsing);
                tabsContentContainer[style][height] = containerHeight + "px";
                tabsContentContainer[offsetHeight];
                activeContent[style][float] = "";
                nextContent[style][float] = "";
            }
            if (hasClass(nextContent, "fade")) {
                setTimeout(function() {
                    addClass(nextContent, showClass);
                    emulateTransitionEnd(nextContent, triggerShow);
                }, 20);
            } else {
                triggerShow();
            }
        };
        if (!tabs) return;
        tabs[isAnimating] = false;
        var getActiveTab = function() {
            var activeTabs = getElementsByClassName(tabs, active), activeTab;
            if (activeTabs[length] === 1 && !hasClass(activeTabs[0][parentNode], "dropdown")) {
                activeTab = activeTabs[0];
            } else if (activeTabs[length] > 1) {
                activeTab = activeTabs[activeTabs[length] - 1];
            }
            return activeTab;
        }, getActiveContent = function() {
            return queryElement(getActiveTab()[getAttribute]("href"));
        }, clickHandler = function(e) {
            var href = e[target][getAttribute]("href");
            e[preventDefault]();
            next = e[target][getAttribute](dataToggle) === component || href && href.charAt(0) === "#" ? e[target] : e[target][parentNode];
            !tabs[isAnimating] && !hasClass(next, active) && self.show();
        };
        this.show = function() {
            next = next || element;
            nextContent = queryElement(next[getAttribute]("href"));
            activeTab = getActiveTab();
            activeContent = getActiveContent();
            tabs[isAnimating] = true;
            removeClass(activeTab, active);
            addClass(next, active);
            if (dropdown) {
                if (!hasClass(element[parentNode], "dropdown-menu")) {
                    if (hasClass(dropdown, active)) removeClass(dropdown, active);
                } else {
                    if (!hasClass(dropdown, active)) addClass(dropdown, active);
                }
            }
            bootstrapCustomEvent.call(activeTab, hideEvent, component, next);
            if (hasClass(activeContent, "fade")) {
                removeClass(activeContent, showClass);
                emulateTransitionEnd(activeContent, triggerHide);
            } else {
                triggerHide();
            }
        };
        if (!(stringTab in element)) {
            on(element, clickEvent, clickHandler);
        }
        if (self[height]) {
            tabsContentContainer = getActiveContent()[parentNode];
        }
        element[stringTab] = self;
    };
    supports[push]([ stringTab, Tab, "[" + dataToggle + '="tab"]' ]);
    var Tooltip = function(element, options) {
        element = queryElement(element);
        options = options || {};
        var animationData = element[getAttribute](dataAnimation), placementData = element[getAttribute](dataPlacement), delayData = element[getAttribute](dataDelay), containerData = element[getAttribute](dataContainer), component = "tooltip", classString = "class", title = "title", fade = "fade", div = "div", containerElement = queryElement(options[container]), containerDataElement = queryElement(containerData), modal = getClosest(element, ".modal"), navbarFixedTop = getClosest(element, "." + fixedTop), navbarFixedBottom = getClosest(element, "." + fixedBottom);
        this[animation] = options[animation] && options[animation] !== fade ? options[animation] : animationData || fade;
        this[placement] = options[placement] ? options[placement] : placementData || top;
        this[delay] = parseInt(options[delay] || delayData) || 200;
        this[container] = containerElement ? containerElement : containerDataElement ? containerDataElement : navbarFixedTop ? navbarFixedTop : navbarFixedBottom ? navbarFixedBottom : modal ? modal : DOC[body];
        var self = this, timer = 0, placementSetting = this[placement], tooltip = null, titleString = element[getAttribute](title) || element[getAttribute](dataTitle) || element[getAttribute](dataOriginalTitle);
        if (!titleString || titleString == "") return;
        var removeToolTip = function() {
            self[container].removeChild(tooltip);
            tooltip = null;
            timer = null;
        }, createToolTip = function() {
            titleString = element[getAttribute](title) || element[getAttribute](dataTitle) || element[getAttribute](dataOriginalTitle);
            if (!titleString || titleString == "") return false;
            tooltip = DOC[createElement](div);
            tooltip[setAttribute]("role", component);
            var tooltipArrow = DOC[createElement](div);
            tooltipArrow[setAttribute](classString, "arrow");
            tooltip[appendChild](tooltipArrow);
            var tooltipInner = DOC[createElement](div);
            tooltipInner[setAttribute](classString, component + "-inner");
            tooltip[appendChild](tooltipInner);
            tooltipInner[innerHTML] = titleString;
            self[container][appendChild](tooltip);
            tooltip[setAttribute](classString, component + " bs-" + component + "-" + placementSetting + " " + self[animation]);
        }, updateTooltip = function() {
            styleTip(element, tooltip, placementSetting, self[container]);
        }, showTooltip = function() {
            !hasClass(tooltip, showClass) && addClass(tooltip, showClass);
        }, showTrigger = function() {
            on(globalObject, resizeEvent, self.hide);
            bootstrapCustomEvent.call(element, shownEvent, component);
        }, hideTrigger = function() {
            off(globalObject, resizeEvent, self.hide);
            removeToolTip();
            bootstrapCustomEvent.call(element, hiddenEvent, component);
        };
        this.show = function() {
            clearTimeout(timer);
            timer = setTimeout(function() {
                if (tooltip === null) {
                    placementSetting = self[placement];
                    if (createToolTip() == false) return;
                    updateTooltip();
                    showTooltip();
                    bootstrapCustomEvent.call(element, showEvent, component);
                    !!self[animation] ? emulateTransitionEnd(tooltip, showTrigger) : showTrigger();
                }
            }, 20);
        };
        this.hide = function() {
            clearTimeout(timer);
            timer = setTimeout(function() {
                if (tooltip && hasClass(tooltip, showClass)) {
                    bootstrapCustomEvent.call(element, hideEvent, component);
                    removeClass(tooltip, showClass);
                    !!self[animation] ? emulateTransitionEnd(tooltip, hideTrigger) : hideTrigger();
                }
            }, self[delay]);
        };
        this.toggle = function() {
            if (!tooltip) {
                self.show();
            } else {
                self.hide();
            }
        };
        if (!(stringTooltip in element)) {
            element[setAttribute](dataOriginalTitle, titleString);
            element.removeAttribute(title);
            on(element, mouseHover[0], self.show);
            on(element, mouseHover[1], self.hide);
        }
        element[stringTooltip] = self;
    };
    supports[push]([ stringTooltip, Tooltip, "[" + dataToggle + '="tooltip"]' ]);
    var initializeDataAPI = function(constructor, collection) {
        for (var i = 0, l = collection[length]; i < l; i++) {
            new constructor(collection[i]);
        }
    }, initCallback = BSN.initCallback = function(lookUp) {
        lookUp = lookUp || DOC;
        for (var i = 0, l = supports[length]; i < l; i++) {
            initializeDataAPI(supports[i][1], lookUp[querySelectorAll](supports[i][2]));
        }
    };
    DOC[body] ? initCallback() : on(DOC, "DOMContentLoaded", function() {
        initCallback();
    });
    return {
        Alert: Alert,
        Button: Button,
        Carousel: Carousel,
        Collapse: Collapse,
        Dropdown: Dropdown,
        Modal: Modal,
        Popover: Popover,
        ScrollSpy: ScrollSpy,
        Tab: Tab,
        Tooltip: Tooltip
    };
});
//# sourceMappingURL=vendor.js.map