﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LatihanSebelumTest4.Entities;
using LatihanSebelumTest4.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LatihanSebelumTest4.API
{
    [Route("api/v1/worker")]
    public class WorkerApiController : Controller
    {

        private readonly ExamDbContext DB;

        public WorkerApiController(ExamDbContext db)
        {
            this.DB = db;
        }

        [HttpGet]
        public async Task<IActionResult> Get(WorkerListFilterModel filter)
        {
            var productQueryable = this
                .DB
                .Worker
                .AsQueryable();

            if (string.IsNullOrEmpty(filter.WN) == false)
            {
                productQueryable = productQueryable.Where(Q => Q.Name.Contains(filter.WN) == true);
            }
            //else if (string.IsNullOrEmpty(filter.G.ToString()) == false)
            //{

            //    productQueryable = productQueryable.Where(Q => Q.Name.Contains(filter.G.ToString()) == true);
            //}

            if (filter.Gor == "Male")
            {
                productQueryable = productQueryable.Where(Q => Q.Gender == true);

            }
            else if (filter.Gor == "Female")
            {
                productQueryable = productQueryable.Where(Q => Q.Gender == false);
            }
            //else if (filter.Gor == "All")
            //{
            //    productQueryable = productQueryable.Where(Q => Q.Gender);
            //}


            if (filter.Or == "Gender")
            {
                if (filter.IDesc == true)
                {
                    productQueryable = productQueryable.OrderByDescending(Q => Q.Gender);
                }
                else
                {
                    productQueryable = productQueryable.OrderBy(Q => Q.Gender);
                }
            }
            else if (filter.Or == "Weight")
            {
                if (filter.IDesc == true)
                {
                    productQueryable = productQueryable.OrderByDescending(Q => Q.Weight);
                }
                else
                {
                    productQueryable = productQueryable.OrderBy(Q => Q.Weight);
                }
            }
            else
            {
                if (filter.IDesc == true)
                {
                    productQueryable = productQueryable.OrderByDescending(Q => Q.Name);
                }
                else
                {
                    productQueryable = productQueryable.OrderBy(Q => Q.Name);
                }
            }

            //var maxItemPerPage = 5;
            ////var offset = (filter.P - 1) * maxItemPerPage;
            //var totalProduct = await productQueryable.CountAsync();

            //var totalPage = (int)Math.Ceiling((decimal)totalProduct / maxItemPerPage);

            var workers = await productQueryable
                //.Skip(offset)
                //.Take(maxItemPerPage)
                .Select(Q => new WorkerListViewModel
                {
                    WorkerId = Q.WorkerId,
                    Name = Q.Name,
                    Weight = Q.Weight,
                    Gender = Q.Gender

                })
                .ToListAsync();

            var productGrid = new WorkerListGridViewModel
            {
                Workers = workers
            };

            return Ok(productGrid);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateWorkerViewModel requestedWorker)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Data is invalid!");
            }

            DB.Worker.Add(new Worker
            {
                WorkerId = Guid.NewGuid(),
                Name = requestedWorker.Name,
                Weight = requestedWorker.Weight,
                Gender = requestedWorker.Gender
            });

            await this.DB.SaveChangesAsync();

            return Ok("Data has been successfully added");
        }

    }
}
