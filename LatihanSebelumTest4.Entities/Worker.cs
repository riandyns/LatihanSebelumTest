﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LatihanSebelumTest4.Entities
{
    public partial class Worker
    {
        public Guid WorkerId { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal Weight { get; set; }
        public bool Gender { get; set; }
    }
}
